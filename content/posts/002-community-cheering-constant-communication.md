+++
title = "Episode 002: Community, Cheering, and Constant Communication"
date = "2020-12-05"
author = "Peter Naudus"
cover = ""
tags = ["video", "community", "prayer"]
keywords = ["video", "community", "prayer"]
description = "Today we're going to be learning about prayer. When I was preparing this study, I initially grouped verses 2 and 3 together. However, remembering the name of this series, I limited myself to focusing on a single verse, and I'm so glad I did. There was an important lesson I would have missed if I hadn't taken the time to slow down and consider every word."
showFullContent = false
+++

{{<vimeo 487564542>}}

{{<subscribe>}}

## Introduction


Hello friends and family, grace and peace to you. Thanks for joining me again today in slowing down and meditating on God's inspired word, one verse at a time.

Today we're going to be learning about prayer. When I was preparing this study, I initially grouped verses 2 and 3 together. However, remembering the name of this series, I limited myself to focusing on a single verse, and I'm so glad I did. There was an important lesson I would have missed if I hadn't taken the time to slow down and consider every word.

## Passage

> We give thanks to God always for all of you, constantly  mentioning you in our prayers,
> - 1 Thessalonians 1:2 (ESV)

## We

> We ...

Did you catch the first word, "`we`"? Not only was Paul praying, but he was also praying with Silas and Timothy.

The importance of praying together is something that is often overlooked. However, it is how Jesus taught us to pray. He said:

> Pray then like this: "**Our** Father in heaven,   hallowed be your name. Your kingdom come, your will be done, on earth as it is in heaven. Give **us** this day **our** daily bread, and forgive **us** **our** debts,  as **we** also have forgiven **our** debtors. And lead **us** not into temptation,  but deliver **us** from evil.
> - Matthew 6:9-13 (ESV)

You see, Jesus was teaching us to pray corporately. Personal prayer is of course important as well, but we must not underestimate the importance of praying with other Christians.

## Cheering on Others

God not only wants us to be praying **with** others, but **for** others as well. The first half of the passage reads:

> We give thanks to God always for all of you ...

Who are you cheering on? This passage reminds me of when the Israelites were at war with Amalek ([Exodus 17:8-13](http://ref.ly/Exodus17:8-13)). The Israelites were able to have victory, but only while their leader, Moses held his staff overhead. When his arms got tired and started dropping, the Israelites started experiencing defeat. So men surrounded Moses, supporting his arms so that he could support the Israelites to victory.

Here, back in our passage, Paul is encouraging the Thessalonians. And likewise, we should be looking for people around us that we can support in prayer. Find someone that you can consistently pray for and tell them that you're praying. If you're not sure who to pray for, remember that like Moses, it is often our leaders, those busy supporting others that are in need of the most prayer.

## Constant Communication

Next, moving onto the second half of the verse, we read:

> ... constantly  mentioning you in our prayers,

If your prayer life is anything like mine, when you're going through a trying time, you have no problem obeying the command to "`pray without ceasing`". ([1 Thessalonians 5:17](http://ref.ly/1Thessalonians5:17)). But when that crisis is over, your prayer life returns to its original lackluster levels. Why is that?

We'll be getting into this in more detail towards the end of First Thessalonians, but have you ever looked at this command within its context? Here it is again, with the context of the surrounding verses:

> Rejoice always, pray without ceasing, give thanks in all circumstances; for this is the will of God in Christ Jesus for you.
> - 1 Thessalonians 5:16-18 (ESV)

Did you notice that the command to "`pray without ceasing`" was sandwiched between "`rejoice always`" and "`give thanks in all circumstances`"? Praying without ceasing doesn't mean constantly telling God everything that we want him to do. It is being in consistent communication, and the majority of that talking should be rejoicing and thanking God for what He has done for us.

When putting together this study, it hit me: the only reason I'm more consistent in my prayer life when times are troubled, is because my prayers are mostly consumed with asking God for things. But I will never run out of things to praise God for, and God is constantly showering us with blessings. If the majority of my prayers consist of praise and thanksgiving, I will have no problem praying without ceasing.

## Application and Conclusion

And this is something Paul exemplified for us, as he prayed consistently for the Thessalonians, thanking God and rejoicing for everything that He had done in their lives.

So what did we learn today?

1. Paul prayed together with Silas and Timothy. Furthermore, when Jesus taught us how to pray he taught us corporately
2. Paul championed the Thessalonians, encouraging and praying for them consistently
3. Paul was consistent in his prayers and constant in his praise

{{<subscribe>}}
