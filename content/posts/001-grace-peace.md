+++
title = "Episode 001: Grace and Peace To You"
date = "2020-11-30"
author = "Peter Naudus"
cover = ""
tags = ["video", "grace", "peace"]
keywords = ["video", "grace", "peace"]
description = "Today we're starting a study of 1 Thessalonians by looking at a phrase you've probably seen countless times before, but perhaps you never stopped to consider what it means."
showFullContent = false
+++

{{<vimeo 485297698>}}

{{<subscribe>}}

## Introduction

Hello friends and family! I trust that the Lord is keeping you all safe and healthy. Thank you for joining me today in slowing down and meditating on God's inspired word, one verse at a time.

Today we're starting a study of 1 Thessalonians by looking at a phrase you've probably seen countless times before, but perhaps you never stopped to consider what it means.

## Scripture Reading

> Paul, Silvanus, and Timothy, To the church of the Thessalonians in God the Father and the Lord Jesus Christ: Grace to you and peace. 
> - 1 Thessalonians 1:1, ESV

## Paul, etc

> Paul, Silvanus, and Timothy ...

Here at the beginning Paul first identifies himself as the author followed by Silvanus (who is Silas) and Timothy who were ministering with him.

As a quick aside, note how Paul recognized the importance of community. He was not only in community with the churches that he served, but he also surrounded himself with men who were passionate about their faith.

## A Faithful Church

> ... To the church of the Thessalonians in God the Father and the Lord Jesus Christ ...

This identifies the recipient of the letter: the church at Thessaloniki was a healthy, vibrant church. In this letter, Paul repeatedly commends the church for their faith and faithfulness in serving Christ.

## Grace and Peace

> ... Grace to you and peace

The phrase "grace and peace" appears many times throughout the New Testament. However, I never stopped to dwell on these words until recently.

Let's take each of these words and go in-depth.

### Mercy

We're going to start our discussion about Grace and Peace by talking about ... neither, actually. Because before you can understand Grace, you first need to understand Mercy.

Mercy is when God does not put upon us the judgement that we rightly deserve.

You see, we are have sinned, meaning we have done things that have fallen short of God's perfect standard. That is deserving of judgement, but God is merciful!

> The LORD is gracious merciful, slow to anger and abounding in steadfast love.
> - Psalm 145:8 (ESV) 

### Grace

Grace is the other side of God's love. It is giving us gifts that we don't deserve.

> Let us then with confidence draw near to the throne of grace, that we may receive mercy and find grace to help in time of need.
> - Hebrews 4:16 (ESV) 

So, get this: God is so amazing that he not only shows us mercy by not giving us the punishment we justly deserve. He goes beyond that and helps us when we are in a time of need.

In fact, not only does God help us in times of need, James tell us that every gift is from Him!

> Every good gift and every perfect gift is from above, coming down from the Father of lights ...
> - James 1:17 (ESV)

### Peace

So we talked about mercy and grace, two sides to God's love. Now, how does peace fit into all of this?

> Peace I leave with you, my peace I give to you. Not as the world gives do I give to you. Let not your hearts be troubled, neither let them be afraid.
> - John 14:27 (ESV)

Interesting ... so it seems that peace is a gift ... and there's the connection. Grace is God giving us gifts that we don't deserve and peace is one of those gifts. Cool! Looking at the second half of that verse we see what peace is, it is the absence of fear. In other words, to be fearless.

If we look at the context of John 14, Jesus tells us why can be fearless:

> Let not your hearts be troubled. Believe in God; believe also in me ... I will come again and will take you to myself, that where I am you may be also. 
> - John 14:1,3 (ESV)

So we can have peace and fearlessness because we believe Jesus' promise that he will return and take us to Heaven. In other words, how do we combat fear? We meditate on our salvation. And that, my friends, is so important, God lists that as one of the pieces of the Armor of God: the Helmet of Salvation (Ephesians 6:17)

So the next time you're afraid, meditate on:

> O death where is your victory? O death where is your sting?
> - 1 Corinthians 15:55 (ESV)

You see, it didn't say that "One day God will have victory. One day death will have no sting". 1 Corinthians 15 goes on to say:

> But thanks be to God, who gives us victory through our Lord Jesus Christ.
> - 1 Corinthians 15:57 (ESV)

Jesus conquered death and is victorious.

When we choose to remember this victory, that is putting on the Helmet of Salvation. And that is gives us peace and allows us to be fearless. And that fearlessness is a gift, and that is how grace and peace fit together.

## Application

So what is our part in all of this? It seems like we get to sit back, chill, and let the fearlessness overtake us. Oh, not so my friend, We have a very active roll in this:

Firstly, we need to put on the Helmet of Salvation by meditating on what Jesus did for us on the cross

Secondly, we need to make sure we are inside of God's will. I encourage you sometime to go through and look at the promises God has made concerning safety, for example: 

> In peace I will lie down and sleep, for you alone, LORD, make me dwell in safety.
> - Psalm 4:8 (ESV)

> The name of the LORD is a fortified tower; the righteous run to it and are safe 
> - Proverbs 18:10 (ESV)

> The Lord will rescue me from every evil attack and will bring me safely to his heavenly kingdom. 
> - 2 Timothy 4:8 (ESV)

Study those verses and you'll find:

1. That God provides the safety
2. This first requires us to obey Him by living the way He wants us to live 
3. That sometimes keeping us safe means taking us home.

## Conclusion

Well, friends and family, this concludes this study. So what did we learn today?

1. God is amazing and blesses us with gifts, and that is called Grace
2. One of those gifts is the ability to be fearless
3. And lastly, that fearlessness comes from putting on the Helmet of Salvation and walking in obedience

May grace, peace, and fearlessness abound in your life today.

{{<subscribe>}}
